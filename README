Multimedia Messaging Service Daemon - The Next Generation (mmsd-tng)
****************************

Copyright (C) 2010-2011, Intel Corporation
              2021, Chris Talbot <chris@talbothome.com>

About
===========================
mmsd-tng is a lower level daemon that transmits and recieves MMSes. It works with
both the Modem Manager stack.

Modem Manager specific notes
===========================
Upon start up, mmsd-tng looks for and tracks the state of the modem
that has Modem Messaging (i.e. SMS) capabilities. Since mmsd-tng is a lower
level program, mmsd-tng assumes that other parts of the OS stack manage mobile
connectivity.

Please note that due to limitations of Modem Manager, mmsd-tng does not support
having multiple APNs at the same time (for carriers that seperate MMS APN
from Mobile Data APNs).

Please read "Configuring the Modem Manager Plugin" for configuration.

Compiling mmsd-tng
============================
In order to compile proxy daemon you need following software packages:
	- GCC compiler
	- D-Bus library
	- GLib library
	- Modem Manager Library
	- LibSoup library
	- c-ares DNS resolver library

Installing mmsd-tng
============================

Build tools requirements

When building and testing directly from the repository it is important to
have at least meson version 0.56 or later installed. In addition, some
supported Meson backend is required (e.g. ninja).

To prepare/configure the project for compilation:

    # meson _build

To build the project:

    # meson compile -C _build

Run tests:

    # meson test -C _build

Install:

    # meson install -C _build


Running from within the source code repository
==============================================

Meson will build the binary in builddir (_build, using the examples above), and
can be invoked directly.

  Run with debugging
    # ./_build/mmsdtng -d

General Configuration
===========================
On first run, mmsd-tng will write a settings file at
"$HOME/.mms/$PLUGIN/mms"

IMPORTANT NOTE: If you change settings in this file, mmsd-tng MUST NOT BE RUNNING
                in order for your settings to take effect!

This settings file use sane defaults, but you can change them:

UseDeliveryReports
        Whether you want delivery reports for MMSes you send

AutoCreateSMIL
	mmsd-tng will attempt to create SMIL for any outgoing MMS without SMIL

TotalMaxAttachmentSize
        The maximum size all of your attachments can be before mmsd-tng rejects it.
        This value is in bytes.
        NOTE: This value is carrier specific! Changing this value to a higher
              number may cause your carrier to silently reject MMSes you send.
              CHANGE AT YOUR OWN RISK!

MaxAttachments
        The maximum number of attachments allowed before mmsd-tng rejects it.
        NOTE: This value is carrier specific! Changing this value to a higher
              number may cause your carrier to silently reject MMSes you send.
              CHANGE AT YOUR OWN RISK!

ForceCAres
        Force the C-Ares backend even if systemd is present
        NOTE: This value can only be changed via manual file editting, and only
              while mmsd-tng is not running.

Configuring the Modem Manager Plugin
===========================
On first run, mmsd-tng will write a settings file at
"$HOME/.mms/$PLUGIN/mms"

It will then attempt to look up your carrier settings from:
https://gitlab.gnome.org/GNOME/mobile-broadband-provider-info

If it cannot find it, I highly suggest for you to add your carrrier settings here.

IMPORTANT NOTE: If you change any settings through the file,
                mmsd-tng MUST NOT BE RUNNING for the changes to take effect!

                You can change CarrierMMSC, CarrierMMSProxy, or MMS_APN via
                dbus and they will take effect right away, but any
	            messages sent to the mmsd-tng queue need to be processed
	            again. The easiest way to do this is to reset mmsd-tng.
	            But it can be done with the dbus proxy call ProcessMessageQueue().

This settings file needs to be changed before mmsd-tng will connect! The settings
are as follows:

CarrierMMSC
        Get this from your carrier.

        Carrier MMSC Format: "http://mms.example.com"

CarrierMMSProxy
        Get this from your carrier.

        MMS Proxy Format: "proxy.example.com:80", "proxy.example.com", or "NULL"
            In the example, the proxy hostname is "proxy.example.com" and the
            proxy port is "80"
            If you do not set the proxy port (e.g. use "proxy.example.com" ),
            mmsd will default to port 80.
            If you do NOT have a proxy, set this to "NULL"

DefaultModemNumber
	If you have multiple modems, this will allow you to only choose which
	modem to connect to by reading its number. The number MUST be set to
	E.164 Setting this to NULL means mmsd-tng will connect to the first modem
	it finds.

	Unless you have multiple modems (not common), you can leave this set to
	"NULL"

MMS_APN
        Note that at this point, this plugin can only support one bearer at
        a time (this works fine for carriers with a combined Internet/MMS APN
        but will not function with carriers that have two APNS seperating
        the two)

        MMS APN Format: "apn.example.com"

AutoProcessOnConnection
        Tell mmsd-tng to automatically send and recieve messages when the modem
        is connected. This will also allow mmsd-tng to auto send/recieve if the
        modem is disconnected and reconnects, suspends and unsuspends, etc.

        AutoProcessOnConnection Options: "true" or "false"

AutoProcessSMSWAP
        Tell mmsd-tng to automatically check and process SMS WAPs. This can be
        useful if you do not have a chat application that manages MMS or if
        a higher level chat application does not process SMS WAPs to send to
        mmsd-tng.

        AutoProcessSMSWAP Options: "true" or "false"


An example of what you are looking for is here:
https://www.t-mobile.com/support/devices/not-sold-by-t-mobile/byod-t-mobile-data-and-apn-settings

From this:

CarrierMMSC=http://mms.msg.eng.t-mobile.com/mms/wapenc
MMS_APN=fast.t-mobile.com
CarrierMMSProxy=NULL
