Architecture overview
=====================

The MMS daemon implements the Multimedia Message Service with integration
into the oFono and Modem Manager telephony stack.


                    +-----------------------+
                    |  Message application  |
                    +-----------------------+
                                ^
                                | Session D-Bus
                                |
        +------------------------------------------------+
        |                                                |
        |                   MMS daemon                   |
        |                                                |
        +------------------------------------------------+
        |                                                |
        |         Modem Manager / oFono plugin           |
        |                                                |
        +------------------------------------------------+
                                ^
                                | System D-Bus
                                |
        +------------------------------------------------+
        |                                                |
        |     Modem Manager / oFono telephony stack      |
        |                                                |
        +------------------------------------------------+
